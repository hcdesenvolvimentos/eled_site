<?php
/**
 * Template Name: Inicial
 * Description:
 *
 * @package Eled
 */

get_header();

global $configuracao;
$frase1 = $configuracao['opt-frase1'];
$frase2 = $configuracao['opt-frase2'];
$frase3 = $configuracao['opt-frase3'];
$frase4 = $configuracao['opt-frase4'];
$Eled_frase1 = $configuracao['opt-eled-frase1'];
$Eled_frase2 = $configuracao['opt-eled-frase2'];
$Eled_frase3 =$configuracao['opt-eled-frase3'];
$Eled_frase4 = $configuracao['opt-eled-frase4'];

?>
<div id="primary" class="content-area">

   <!-- PÁGINA INICIAL -->
   <div class="pg pg-inicial">

    <div class="container">

        <!-- QUEM SOMOS -->
        <div class="row quem-somos">

            <!-- TEXTO QUEM SOMOS -->
            <div class="col-md-6 correcao-x">
                <div class="titulo text-center">
                    <span>Quem <b>Somos</b></span>
                    <img src="<?php echo get_template_directory_uri(); ?>/img/luz.png">
                </div>
                <p>
                <?php if ( have_posts() ) : while( have_posts() ) : the_post();

                        the_content();

                    endwhile; endif; ?>
                </p>
            </div>

            <!-- VÍDEO YOUTUBE -->
            <div class="col-md-6 correcao-x">
                <div class="titulo-yt text-center">
                    <p><img src="<?php echo get_template_directory_uri(); ?>/img/video.png">Porque a  <b>ELED </b></p>
                </div>

                <div class="embed-responsive embed-responsive-16by9 iframe">
                       <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo $configuracao['opt-embed']; ?>"></iframe>
                       <img src="<?php echo get_template_directory_uri(); ?>/img/minilogo.png" alt="minilogo">
                   </div>
            </div>

        </div>

        <!-- PARCEIROS -->
        <div class="row parceiros">

            <div class="col-md-6">
                <div class="titulo text-center">
                    <span>Clientes</span>
                </div>
            </div>



            <!-- CARROSSEL PARCEIROS -->
            <div class="col-md-12 carrossel-parceiros">
                <div id="carrossel-parceiros-inicial" class="owl-carousel">

                    <?php

                        $parceirosPost = new WP_Query( array( 'post_type' => 'cliente', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );
                        while ( $parceirosPost->have_posts() ) : $parceirosPost->the_post();

                        $foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                        $foto = $foto[0];

                    $linkParceiro = rwmb_meta( 'Eled_link_parceiros' );

                    ?>
                        <div class="item">
                          <a href="<?php echo $linkParceiro; ?>" target="_blank">  <img src="<?php echo "$foto"; ?>" class="img-responsive center-block" alt="<?php echo get_the_title(); ?>"></a>
                        </div>

                    <?php endwhile; ?>

                </div>
            </div>

       </div>

        <!-- VANTAGENS -->
        <div class="row vantagens">
            <div class="row">
                <div class="col-md-6">
                    <div class="titulo text-center">
                         <span>Porque não comprar <b>Led</b> no escuro?</span>
                    </div>
                </div>
            </div>

             <!-- ITENS VANTAGENS LED -->
            <div class="row" >
                <div class="col-md-6 a item-vantagem">
                    <div class="row item">

                        <div class="col-md-3">
                            <div class="quadrado">
                                <div class="pergunta"></div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <p><?php echo $frase1; ?></p>
                        </div>

                    </div>
                </div>

                <div class="col-md-6 a item-vantagem2">
                    <div class="row item">

                        <div class="col-md-3">
                            <div class="quadrado">
                                <div class="check"></div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <p><?php echo $frase2; ?></p>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 a item-vantagem">
                    <div class="row item">

                        <div class="col-md-3">
                            <div class="quadrado">
                                <div class="mais"></div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <p><?php echo $frase3; ?></p>
                        </div>

                    </div>
                </div>

                <div class="col-md-6 a item-vantagem2">
                    <div class="row item">

                        <div class="col-md-3">
                            <div class="quadrado">
                                <div class="lixeira"></div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <p><?php echo $frase4; ?></p>
                        </div>

                    </div>
                </div>

            </div>

        </div>

        <!-- IMAGEM PORQUE A LED -->
        <div class="imagem-fundo">

            <div class="row">
                <div class="col-md-6">
                    <div class="titulo2 text-center">
                        <span>Porque a <b>Eled</b></span>
                    </div>
                </div>
            </div>

            <!-- ITENS VANTAGENS ELED -->
            <div class="row">

                <div class="col-md-6 a correcao-x">
                    <div class="row">
                        <div class="col-md-2 mh90px">
                            <div class="editar"></div>
                        </div>
                        <div class="col-md-10">
                            <p><?php echo $Eled_frase1 ?></p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 a correcao-x">
                    <div class="row">
                        <div class="col-md-2 mh90px">
                            <div class="mensagem"></div>
                        </div>
                        <div class="col-md-10">
                            <p><?php echo $Eled_frase2 ?></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 a correcao-x">
                    <div class="row">
                        <div class="col-md-2 mh90px">
                            <div class="positivo"></div>
                        </div>
                        <div class="col-md-10">
                            <p><?php echo $Eled_frase3 ?></p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 a correcao-x">
                    <div class="row texto">
                        <div class="col-md-2 mh90px">
                            <div class="perfil"></div>
                        </div>
                        <div class="col-md-10">
                            <p><?php echo $Eled_frase4 ?></p>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- DEPOIMENTOS -->
        <div class="row depoimentos">

            <div class="col-md-12">
                <div id="carrossel-depoimentos-inicial" class="owl-carousel">

                        <?php

                            $depoimentosPost = new WP_Query( array( 'post_type' => 'depoimentos', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );

                            while ( $depoimentosPost->have_posts() ) : $depoimentosPost->the_post();

                            $foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                            $foto = $foto[0];



                        ?>

                        <div class="item">

                            <div class="row">

                                <!-- FOTO -->
                                <div class="col-md-3">
                                    <div class="foto" style="background: url(<?php echo $foto  ?>);"></div>
                                </div>

                                <!-- COMENTÁRIO -->
                                <div class="col-md-9 correcao-x">
                                    <div class="comentario">
                                        <p>“ <?php echo get_the_content(); ?> ”   <strong>-</strong>  <span><?php echo get_the_title(); ?>  <?php the_time('j \d\e F \d\e Y') ?></span></p>
                                    </div>
                                </div>

                            </div>

                        </div>


                        <?php endwhile; ?>

                </div>

                <!-- BOTÕES DE NAVEGAÇÃO -->
                <button class="navegacaoCarrosseldepoimentoFrent hidden-xs"><i class="fa fa-angle-left"></i></button>
                <button class="navegacaoCarrosseldepoimentoTras hidden-xs"><i class="fa fa-angle-right"></i></button>

            </div>

        </div>

        <!-- NOVIDADES -->
        <div class="row novidades">
            <div class="row">

                <div class="col-md-6">

                    <div class="titulo text-center">
                        <span>Últimas <b>novidades?</b></span>
                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-md-10 col-md-offset-1">


                        <div class="row">

                                <?php

                                    $destaquesPost = new WP_Query( array( 'post_type' => 'post', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => 3 ) );
                                    while ( $destaquesPost->have_posts() ) : $destaquesPost->the_post();

                                    $foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                                    $foto = $foto[0];

                                 ?>
                                    <!-- NOVIDADE -->
                                    <div class="col-md-4">

                                <a href="<?php echo get_permalink(); ?>" title="">
                                        <div class="imagem" style="background: url(<?php echo "$foto"; ?>">
                                    </div>

                                    <div class="data">
                                        <div class="row">
                                            <div class="col-xs-5 calendario">
                                                <span></span>
                                            </div>
                                            <div class="col-xs-7 data-texto">
                                                <strong> <?php the_time('j \d\e F') ?></strong>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="descricao">
                                        <h2><?php echo get_the_title(); ?></h2>
                                    </div>

                                </a>
                            </div>
                                 <?php endwhile; ?>

                            <div class="ver-mais">
                                <a href="<?php echo home_url('/novidades'); ?>"><i class="fa fa-angle-double-right"></i></a>
                            </div>
                    </div>

                </div>

            </div>

        </div>

    </div>

   </div>

</div><!-- #primary -->


<?php get_footer(); ?>