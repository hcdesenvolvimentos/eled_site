<?php
/**
 * Template Name: Parceiros
 * Description: 
 *
 * @package Eled
 */

get_header();

?>

<?php while ( have_posts() ) : the_post();  ?>

<div class="pg pg-parceiros">

        <div class="imagem-top topo-parceiros">

        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="titulo-internas">
                    <span>Parceiros</span>
                </div>
            </div>
        </div>

        <div class="container">

            <!-- SKEW PARCEIROS-->
            <div class="skew-on">
                <div class="skew-off">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- <div class="titulo-skew">
                                <span>Confira nossos <b>Parceiros</b></span>
                            </div> -->
                        </div>
                        <div class="col-md-12">
                            <ul class="lista">

                                <?php                               

                                    // EXECUTA O LOOP DE ITENS DO PARCEIROS 
                                    $parceiros = new WP_Query( array( 'post_type'       => 'parceiros', 
                                                                     'orderby'          => 'rand',  
                                                                     'posts_per_page'   => -1,                                                               
                                                                    )
                                                            );                                                              

                                ?>

                                <?php 
                                    
                                    // ENQUANTO HOUVER ITENS NO LOOP
                                    while ( $parceiros->have_posts() ) : $parceiros->the_post();
                                    
                                    $post_id = get_the_ID(); 
                                    
                                ?>

                                <?php
                                    
                                    $thumb  = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
                                    $thumb  = $thumb[0];

                                    $linkParceiro = rwmb_meta( 'Eled_link_parceiros' );

                                ?>                              


                                <a href="<?php echo $linkParceiro; ?>" target="_blank"><li><img src="<?php echo $thumb?>" class="img-responsive"></li></a>

                                <?php endwhile; wp_reset_query(); ?>  
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
           <?php 
                $foto = $configuracao['opt-parceiros-foto']['url'];
                $frases = $configuracao['opt-parceiros-frase-banner'];
                $frase = explode("|", $frases);
            ?>
            <!-- BANNER -->
            <div class="row">
                <div class="col-md-12">
                    <div class="banner-parceiros" style="background:url(<?php echo  $foto ?>);background-size:cover!important;">                      
                        <div class="texto-banner">
                            <span><b><?php echo $frase[0] ?></b><?php echo $frase[1]  ?></span>
                        </div>

                    </div>
                </div>
            </div>
        
                 

           <?php 
               
                $frases2 = $configuracao['opt-parceiros-frase'];
                $frase2 = explode("|", $frases2);
            ?>

                <!-- FRASE -->
                <div class="row frase">
                    <div class="col-md-12">
                        <div class="texto-frase">
                            <span><b><?php echo $frase2[0]; ?></b><?php echo $frase2[1]; ?></span>
                        </div>
                    </div>
                </div>


        </div>

</div>

<?php endwhile; ?>

<?php get_footer(); ?>