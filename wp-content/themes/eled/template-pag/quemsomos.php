<?php
/**
 * Template Name: Quem Somos
 * Description: 
 *
 * @package Eled
 */

get_header();

?>
<!-- PÁGINAS QUEM SOMOS  -->
    <div class="pg pg-quem-somos">

        <div class="imagem-top topo-quem-somos">
            <div class="efeito-sombra"></div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="titulo-internas">
                    <span>Quem  <b>Somos</b></span>
                </div>
            </div>
        </div>

        <div class="container">

            <!-- QUEM SOMOS -->
            <div class="row quem-somos">

                <!-- TEXTO QUEM SOMOS -->
                <div class="col-md-6 correcao-x"> 

                    <?php if ( have_posts() ) : while( have_posts() ) : the_post();
                    the_content();
                    endwhile; endif; ?>

                </div>

                <!-- VÍDEO YOUTUBE -->
                <div class="col-md-6 correcao-x">
                    <div class="titulo-yt text-center">
                    <?php 
                        
                       $videoTitulo = explode("|", $configuracao['opt-tituloVideo']);
                     ?>
                        <p><img src="<?php bloginfo('template_directory'); ?>/img/video.png"><?php echo $videoTitulo[0] ?> <b><?php echo $videoTitulo[1] ?></b></p>
                    </div>

                    <div class="embed-responsive embed-responsive-16by9 iframe">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo $configuracao['opt-embed']; ?>"></iframe>
                        <img src="<?php bloginfo('template_directory'); ?>/img/minilogo.png" alt="minilogo">
                    </div>
                </div>
            </div>

             <!-- SKEW QUEM SOMOS -->
            <div class="skew-on">
                <div class="skew-off">
                    <div class="caixa">                 <!-- ITENS VANTAGENS ELED -->
                        <div class="row">

                            <div class="col-md-6 a correcao-x">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="bg-circulo">
                                            <div class="editar2"></div>
                                        </div>

                                    </div>
                                    <div class="col-md-9">
                                        <p><?php echo $configuracao['opt-quem-somos-fraseA'] ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 a correcao-x">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="bg-circulo">
                                            <div class="mensagem2"></div>
                                        </div>

                                    </div>
                                    <div class="col-md-9">
                                       <p><?php echo $configuracao['opt-quem-somos-fraseB'] ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 a correcao-x">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="bg-circulo">
                                            <div class="positivo2"></div>
                                        </div>

                                    </div>
                                    <div class="col-md-9">
                                       <p><?php echo $configuracao['opt-quem-somos-fraseC'] ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 a correcao-x">
                                <div class="row texto">
                                    <div class="col-md-3">
                                        <div class="bg-circulo">
                                            <div class="perfil2"></div>
                                        </div>

                                    </div>
                                    <div class="col-md-9">
                                        <p><?php echo $configuracao['opt-quem-somos-fraseD'] ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- FRASES QUEM SOMOS 1 -->
            <?php while ( have_posts() ) : the_post(); ?>

            <?php 
               $frases = rwmb_meta('Eled_page_frases');
               foreach ($frases as $frase) {                  
            ?>

            <div class="row frase">
                <div class="col-md-12">
                    <div class="texto-frase">
                        <span><?php echo $frase; ?></span>
                    </div>
                </div>
            </div>

            <?php } ?>

            <?php endwhile; ?>

            
            <!-- CARROSSEL QUEM SOMOS -->
            <div class="row carrossel-imagens">

                <div class="col-md-12">

                    <div id="carrossel-quem-somos" class="owl-carousel">                        

                        <?php 

                            while ( have_posts() ) : the_post(); 

                                $paginafotos = rwmb_meta( 'Eled_page_foto', 'type=image&size=full' );

                                foreach ( $paginafotos as $paginafoto )
                                {
                        ?>

                       
                        <!-- FOTO -->
                        <a id="example1" href="<?php echo $paginafoto['url']; ?>" title="">
                            
                            <div class="item">
                            
                                <img src="<?php echo $paginafoto['url']; ?>" alt="">
                            </div>
                        </a>
                        
                                <?php } ?>
                        
                            <?php endwhile; ?>                     

                    </div>

                    <div class="text-right correcao-x bg-azul">
                        <!-- BOTÕES DE NAVEGAÇÃO -->
                        <button class="navegacaoCarrosselQuemTras hidden-xs"><i class="fa fa-angle-left"></i></button>
                        <button class="navegacaoCarrosselQuemFrente hidden-xs"><i class="fa fa-angle-right"></i></button>
                    </div>

                </div>

            </div>
        </div>
    </div>


<?php get_footer(); ?>