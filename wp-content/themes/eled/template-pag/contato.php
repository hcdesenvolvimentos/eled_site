<?php
/**
 * Template Name: Contato
 * Description:
 *
 * @package Eled
 */

get_header();

?>
<div class="pg pg-contato">

    <div class="imagem-top topo-contato">
        <div class="efeito-sombra"></div>
    </div>

    <!-- TÍTULO -->
    <div class="row">
        <div class="col-md-12">
            <div class="titulo-internas">
                <span>Contato <b>Eled</b></span>
            </div>
        </div>
    </div>

    <div class="container">

        <!-- FRASE TOPO NOVIDADES -->
        <div class="row frase-novidades">

            <div class="col-md-12 correcao-x text-center">
                <p>Entre em contato conosco</p>
            </div>

        </div>

        <!-- INFORMAÇÕES DE CONTATO -->
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-8 col-md-offset-2 alinhamento">
                    <div class="caixa">
                        <div class="conteudo"><img src="<?php bloginfo('template_directory'); ?>/img/lampada.png"> Eled <strong>Light</strong></div>

                        <span><img src="<?php bloginfo('template_directory'); ?>/img/telefone.png">  <?php echo $configuracao['opt-telefone']; ?></span>
                        <span><img src="<?php bloginfo('template_directory'); ?>/img/e-mail.png">  <?php echo $configuracao['opt-emailContato']; ?></span>
                        <span><img src="<?php bloginfo('template_directory'); ?>/img/e-ndereco.png">  <?php echo $configuracao['opt-ruaNumero']; ?></span>

                    </div>
                </div>
            </div>

            <!-- FORMULÁRIO DE CONTATO -->


            <div class="col-md-6">
                <div class="col-md-8 col-md-offset-2 correcao-x">
                    <div class="c-aixa">

                        <?php
                           echo do_shortcode('[contact-form-7 id="4" title="Formulário de contato"]');
                        ?>


                    </div>
                </div>
            </div>
        </div>

        <!-- SKEW COM INPUT EMAIL -->
        <div class="skew-img">
            <div class="skew-conteudo">
               <?php 
                    $fraseContato  = $configuracao['opt-contato-frase'];
                    $fraseContato  = explode("|", $fraseContato );

                ?>
                <span><?php echo $fraseContato[0] ?> <strong><?php echo $fraseContato[1] ?></strong></span>
                <?php //echo do_shortcode('[contact-form-7 id="76" title="Formulário de Newsletter"]'); ?>
                <!--START Scripts : this is the script part you can add to the header of your theme-->
                <script type="text/javascript" src="http://eled.pixd.com.br/wp-includes/js/jquery/jquery.js?ver=2.6.19"></script>
                <script type="text/javascript" src="http://eled.pixd.com.br/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.6.19"></script>
                <script type="text/javascript" src="http://eled.pixd.com.br/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.6.19"></script>
                <script type="text/javascript" src="http://eled.pixd.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.6.19"></script>
                <script type="text/javascript">
                    /* <![CDATA[ */
                    var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://eled.pixd.com.br/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
                    /* ]]> */
                </script><script type="text/javascript" src="http://eled.pixd.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.6.19"></script>
                <!--END Scripts-->

                <div class="widget_wysija_cont html_wysija"><div id="msg-form-wysija-html566f16647ccd9-2" class="wysija-msg ajax"></div><form id="form-wysija-html566f16647ccd9-2" method="post" action="#wysija" class="widget_wysija html_wysija">
                <label for="seuemail" class="hidden">Email</label>

                <input type="text" name="wysija[user][email]" class="text wysija-input validate[required,custom[email]]" title="Email"  value="" placeholder="email"/>



                <span class="abs-req">
                    <input type="text" name="wysija[user][abs][email]" class="text wysija-input validated[abs][email]" value="" placeholder="email"/>
                </span>


                <input class="botao wysija-submit wysija-submit-field" type="submit" value="" />

                <input type="hidden" name="form_id" value="2" />
                <input type="hidden" name="action" value="save" />
                <input type="hidden" name="controller" value="subscribers" />
                <input type="hidden" value="1" name="wysija-page" />


                <input type="hidden" name="wysija[user_list][list_ids]" value="1" />

                </form></div>

                </form></div>
            </div>
        </div>
    </div>

</div>


<?php get_footer(); ?>