<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eled_site
 */

get_header(); ?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		
	<!-- PÁGINAS NOVIDADES  -->
	<div class="pg pg-novidades">

		<div class="imagem-top topo-novidades">
			<div class="efeito-sombra"></div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="titulo-internas">
					<span>Novidades <b>Eled</b></span>
				</div>
			</div>
		</div>

		<div class="container">

			<!-- FRASE TOPO NOVIDADES -->
			<div class="row frase-novidades">

				<div class="col-md-12 correcao-x text-center">
					<p>Confira as últimas <b>novidades</b></p>
				</div>

			</div>

			<!-- MENU LATERAL -->
			<?php get_sidebar(); ?>

		</div>

	</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
