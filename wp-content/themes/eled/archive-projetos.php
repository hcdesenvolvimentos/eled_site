<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eled_site
 */

get_header(); ?>

<div class="pg pg-projetos">

	<div class="imagem-top topo-projetos">
		<div class="efeito-sombra">

		</div>
	</div>

	<!-- NOSSOS PROJETOS -->
	<div class="container c-fluid">

		<div class="titulo-internas">
			<span>Nossos <b>Projetos</b></span>
		</div>

		<div class="row projetos">

			<div class="col-sm-3">

				<ul class="sub-menu" >
				<?php
				$categorias = get_terms( 'categoriaProjetos', 'orderby=countDesc&hide_empty=0' );
				foreach ($categorias as $categoria):
					if ($categoria->slug == "residencial" || $categoria->slug == "comercial"):
				?>
					<li><a href="<?php echo get_term_link($categoria,'categoriaProjetos'); ?>" class="botaoCat"><?php echo $categoria->name; ?> <i class="fa fa-angle-double-right"></i></a></li>
				<?php
					endif;
				endforeach;
				?>
				</ul>
				<!-- <div class="legenda">
					<ul>
						<li>
							<div class="row">
								<div class="col-xs-3">

									<div class="icone-imagem"> -->
										<!-- <img src="<?php bloginfo(/*'template_directory'*/); ?>/img/dinheiro2.png"> -->
									<!-- </div>

								</div>
								<div class="col-xs-9">
									<span>Sedtincidunt luctus.</span>
								</div>
							</div>

						</li>

						<li>
							<div class="row">
								<div class="col-xs-3">

									<div class="icone-imagem"> -->
										<!-- <img src="<?php bloginfo(/*'template_directory'*/); ?>/img/a-viao.png"> -->
									<!-- </div>

								</div>
								<div class="col-xs-9">
									<span>Suspendisseurna.</span>
								</div>
							</div>

						</li>

						<li>
							<div class="row">
								<div class="col-xs-3">

									<div class="icone-imagem"> -->
										<!-- <img src="<?php bloginfo(/*'template_directory'*/); ?>/img/ponto.png"> -->
									<!-- </div>

								</div>
								<div class="col-xs-9">
									<span>Vivamus orci leo tincidunt.</span>
								</div>
							</div>

						</li>

						<li>
							<div class="row">
								<div class="col-xs-3">

									<div class="icone-imagem"> -->
										<!-- <img src="<?php bloginfo(/*'template_directory'*/); ?>/img/medalha.png"> -->
									<!-- </div>

								</div>
								<div class="col-xs-9">
									<span>Vicamus orci leo tincidun</span>
								</div>
							</div>

						</li>

					</ul>

				</div> -->
				<!-- <div class="caixa-texto">
					<p>Pellentesque auctor tincidunt nulla nec sodales.Fusce et cursus erat.Morbi tellus neque, pretium ut suscipit</p>
				</div> -->
			</div>

			<div class="col-sm-9" id="area-projetos">

					<div id="projetos-container" class="projetos-container">

						<ul class="projetos-grid">

							<?php
								// ENQUANTO HOUVER ITENS NO LOOP
								while ( have_posts() ) : the_post();
								$post_id = get_the_ID();

							?>

							<!-- PROJETO -->
							<li>
								<?php echo '<a href="' . get_permalink() . '" title="' . __('Continue Reading ', 'eled') . get_the_title() . '"' ?> data-cat="comercial" class="teste2" rel="bookmark">
									<div class="projeto">

										<div class="sombra">
											<?php
												$thumb 	= wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
												$thumb  = $thumb[0];
											?>
											<div  <?php echo 'class="teste foto-projeto foto' . get_the_time('Y', $post_id) . '"' ?> data-categoria="comercial" style="background: url('<?php echo $thumb; ?>');"></div>
										</div>

										<div class="dados-projeto text-center">
											<?php the_title( sprintf( '<h2 class="entry-title">'), '</a><span> | ' );  echo get_the_time('Y', $post_id);?></span></h2>
										</div>

									</div>
								</a>
							</li>


							<?php

								endwhile;
							?>
						</ul>

					</div>

			</div>

		</div>

		<?php pagination(); ?>

		<!-- <ul class="paginador">
			<li><a href=""><i class="fa fa-angle-left"></i></a></li>
			<li><a href="">1</a></li>
			<li><a href="">2</a></li>
			<li><a href="">3</a></li>
			<li><a href="">4</a></li>
			<li><a href="">5</a></li>
			<li><a href=""><i class="fa fa-angle-right"></i></a></li>
		</ul> -->

	</div>

</div>


<?php get_footer(); ?>

<!-- <script type="text/javascript">

	$(function(){

		$(".sub-menu").on('click','#mudarCat',function(){

			var cat = $(this).attr("data-cat");

			$(".projeto").parent().hide();

			$('.teste').each(function(e){

				var div = $(this);

				if(div.attr("data-categoria") == cat){

					$(div).parent().parent().parent().parent().show();

				}

			});

		});

		var divPosition = $('#area-projetos').offset();

		$('.botaoAno').click(function () {
		        $('html, body').animate({
		            scrollTop: divPosition.top
		        }, 1000);
	    });

	});

</script>
 -->