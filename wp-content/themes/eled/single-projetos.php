<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package eled_site
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<!-- PÁGINA PROJETO -->
	<div class="pg pg-projeto" style="display:;">

		<div class="imagem-top topo-projetos">
			<div class="efeito-sombra">

			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="titulo-internas">
					<span>Projetos <b>Eled</b></span>
				</div>
			</div>
		</div>

		<div class="container">

			<!-- CARROSSEL -->
			<div class="row carrossel">

				<div id="carrossel-projeto" class="owl-carousel">

					<?php
						while ( have_posts() ) : the_post();


							$fotoprojetos = rwmb_meta( 'Eled_projetos_plano_fundo', 'type=image&size=full' );
							foreach ( $fotoprojetos as $fotoprojetos )
							{

					?>
					<div class="item" style="background: url(<?php echo $fotoprojetos['url']; ?>);">

					</div>

							<?php } ?>
						<?php endwhile; ?>


				</div>

				<div class="carrossel-conteudo col-md-9 correcao-x">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">

							<!-- TITULO PROJETO -->
							<div class="dados-projeto">
								<h2><strong><?php echo get_the_title(); ?></strong> <small>| <?php echo date("Y", strtotime($post->post_date)); ?></small> <span><?php echo rwmb_meta('Eled_projetos_trabalho_realizado'); ?></span> </h2>
							</div>


							<!-- ITENS DO PROJETOS -->
							<div class="item-projeto">
								<div class="row">
									<div class="col-sm-2">
										<!-- <img src="<?php echo get_template_directory_uri(); ?>/img/dinheiro.png" alt=""> -->
									</div>
									<div class="col-sm-10">
										<i class="fa fa-info-circle fa-3x fa-pull-left fa-border"></i>
										<p><?php echo get_the_content(); ?></p>
									</div>
								</div>
							</div>

							<!-- <div class="item-projeto">
								<div class="row">
									<div class="col-sm-2">
										<img src="<?php echo get_template_directory_uri(); ?>/img/enviar.png" alt="">
									</div>
									<div class="col-sm-10">
										<p>Sed tincidunt luctus urna, hendrerit lobortis magna posuere nec dolor sit amet, consectetur adipiscing.</p>
									</div>
								</div>
							</div>
 -->
							<!-- <div class="item-projeto">
								<div class="row">
									<div class="col-sm-2">
										<img src="<?php echo get_template_directory_uri(); ?>/img/interrogacao.png" alt="">
									</div>
									<div class="col-sm-10">
										<p>Sed tincidunt luctus urna, hendrerit lobortis magna posuere nec dolor sit amet, consectetur adipiscing.</p>
									</div>
								</div>
							</div> -->

							<!-- <div class="item-projeto">
								<div class="row">
									<div class="col-sm-2">
										<img src="<?php echo get_template_directory_uri(); ?>/img/certificado.png" alt="">
									</div>
									<div class="col-sm-10">
										<p>Sed tincidunt luctus urna, hendrerit lobortis magna posuere nec dolor sit amet, consectetur adipiscing.</p>
									</div>
								</div>
							</div>
 -->
						</div>
					</div>
				</div>

				<div class="carrossel-caixa col-md-3">
					<div class="titulo-conteudo">
						<span><img src="<?php echo get_template_directory_uri(); ?>/img/info.png" alt=""> Sobre o cliente</span>
					</div>

					<div class="conteudo">
						<p>
							<?php echo rwmb_meta('Eled_projetos_detalhe_cliente'); ?>
						</p>
					</div>

				</div>

			</div>

			<!-- CARROSSEL IMAGENS DO PROJETO -->
			<div class="row carrossel-imagens">
				<div class="col-md-12">
					<div id="carrossel-imagens-projeto" class="owl-carousel">

						<?php

							while ( have_posts() ) : the_post();

								$projetosfotos = rwmb_meta( 'Eled_projetos_foto', 'type=image&size=full' );
								foreach ( $projetosfotos as $projetosfoto )
								{

						?>
						<!-- FOTO -->
						<a id="example1" rel="gallery1" href="<?php echo $projetosfoto['url']; ?>" title="">

							<div class="item">

								<img src="<?php echo $projetosfoto['url']; ?>" alt="Galeria de imagens do projeto">
							</div>
						</a>


								<?php } ?>

							<?php endwhile; ?>
					</div>
					<div class="text-right correcao-x">
						<!-- BOTÕES DE NAVEGAÇÃO -->
						<button class="navegacaoCarrosselProjeTras hidden-xs"><i class="fa fa-angle-left"></i></button>
						<button class="navegacaoCarrosselProjeFrente hidden-xs"><i class="fa fa-angle-right"></i></button>
					</div>

				</div>
			</div>




			<div class="row">
				<div class="col-md-12 ">
					<div class="titulo-internas">
						<span>Próximos <b>projetos</b></span>
					</div>
				</div>
			</div>

			<!-- PROXIMOS PROJETOS -->
			<div class="row">
				<?php

					$trabalhosPost = new WP_Query(array(
					    'post_type' => 'projetos',
					    'tax_query' 		=> array(
														array(
															'taxonomy' => 'categoriaProjetos',
															'field'    => 'slug',
															'terms'    => 'futuros',
														)
													)
					    )
					);

					$tituloatual = get_the_title();
					$dados[0] = get_next_post();
					$dados[1] = get_previous_post();
					$foto1 = wp_get_attachment_image_src( get_post_thumbnail_id($dados[0]->ID), 'full' );
					$foto1 = $foto1[0];
					$foto2 = wp_get_attachment_image_src( get_post_thumbnail_id($dados[1]->ID), 'full' );
					$foto2 = $foto2[0];

					if(get_the_title($dados[0]->ID) != get_the_title()){
				?>


				<div class="col-md-6 coluna">

					<div class="projeto" style="background: url(<?php echo $foto1;?>)">
						<div class="transparent">
							<div class="conteudo correcao-x">
								<h3><?php echo get_the_title($dados[0]->ID); ?></h3>
								<span>| <?php echo get_the_time('j \d\e F', $dados[0]->ID); ?></span>
								<p><?php echo get_the_content($dados[0]->ID); ?></p>
								<?php echo '<a href="' . get_permalink($dados[0]->ID) . '" title="' . __('Continue Reading ', 'eled') . get_the_title() . '" rel="bookmark">'; ?><img src="<?php echo get_template_directory_uri(); ?>/img/arrow-left.png" alt=""> Ver projeto</a>
							</div>
						</div>
					</div>
				</div>

				<?php }else{ echo '<div class="col-md-6 coluna"></div>';?>


				<?php }if (get_the_title($dados[1]->ID) != get_the_title()) { ?>

				<div class="col-md-6 coluna">

					<div class="projeto" style="background: url(<?php echo $foto2;?>)">
						<div class="transparent">
							<div class="conteudo correcao-x">
								<h3><?php echo get_the_title($dados[1]->ID); ?></h3>
								<span>| <?php echo get_the_time('j \d\e F', $dados[1]->ID); ?></span>
								<p><?php echo get_the_content($dados[1]->ID); ?></p>
								<?php echo '<a href="' . get_permalink($dados[1]->ID) . '" title="' . __('Continue Reading ', 'eled') . get_the_title() . '" rel="bookmark">'; ?>Ver projeto <img src="<?php echo get_template_directory_uri(); ?>/img/arrow-right.png" alt=""></a>
							</div>
						</div>
					</div>
				</div>

				<?php } ?>

			</div>

		</div>

	</div>


	</div><!-- #primary -->




<?php get_footer(); ?>
