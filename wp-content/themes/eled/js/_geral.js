
	$(function(){
			/*****************************************
		        *  CARROSSEL DA PÁGINA ÍNICIAL
		    *****************************************/
			    $("#carrossel-inicial").owlCarousel({
			        items : 1,
			        dots: false,
			        loop: true,
			        lazyLoad: true,
			        mouseDrag: false,
			        autoplay:true,
				    autoplayTimeout:10000,
				    autoplayHoverPause:true,
				    animateOut: 'fadeOut',
				    smartSpeed: 450,
				    autoplaySpeed: 4000
			    });
			    	var carrosselInicial = $("#carrossel-inicial").data('owlCarousel');

					$('.navegacaoCarrosselTopTras').click(function(){ carrosselInicial.prev(); });
					$('.navegacaoCarrosselTopFrente').click(function(){ carrosselInicial.next(); });

		        	var num = 2;

			    	if (num % 2 == 0) {
			    		$( '#carrossel-inicial .item' ).css( "background-position", "50% 0%" );
			    		$('#carrossel-inicial .item').animate({
			    			'background-position-x': '50%',
							'background-position-y': '100%'
						}, 60000, 'linear');
						num++;
			    	}else{
			    		$( '#carrossel-inicial .item' ).css( "background-position", "50% 100%" );
			    		$('#carrossel-inicial .item').animate({
			    			'background-position-x': '50%',
							'background-position-y': '0%'
						}, 60000, 'linear');
						num++;
			    	}


			/*****************************************
		    *  			CARROSSEL PARCEIROS
		    *****************************************/
			    $("#carrossel-parceiros-inicial").owlCarousel({
			        items : 5,
			        dots: false,
			        loop: true,
			        lazyLoad: true,
			        mouseDrag: true,
			        autoplay:true,
				    autoplayTimeout:5000,
				    autoplayHoverPause:true,
				    animateOut: 'fadeOut',
				    smartSpeed: 450,
				    autoplaySpeed: 4000,
				    responsiveClass:true,
			        responsive:{
						            0:{
						                items:1
						            },
						            600:{
						                items:3
						            },
						            1000:{
						                items:5
						            }
				        		}
			    });


		    	/*****************************************
		            *  CARROSSEL DA PÁGINA ÍNICIAL
		        *****************************************/

		    	    $("#carrossel-depoimentos-inicial").owlCarousel({
		    	        items : 1,
		    	        dots: false,
		    	        loop: true,
				        lazyLoad: true,
				        mouseDrag: true,
				        autoplay:true,
					    autoplayTimeout:5000,
					    autoplayHoverPause:true,
					    animateOut: 'fadeOut',
					    smartSpeed: 450,
					    autoplaySpeed: 4000
		    	    });
			    	var carrosseldepo = $("#carrossel-depoimentos-inicial").data('owlCarousel');

					$('.navegacaoCarrosseldepoimentoFrente').click(function(){ carrosseldepo.prev(); });
					$('.navegacaoCarrosseldepoimentoTras').click(function(){ carrosseldepo.next(); });

				/*****************************************
			        *  CARROSSEL DA PÁGINA PROJETO
			    *****************************************/
				    $("#carrossel-projeto").owlCarousel({
				        items : 1,
				        dotsEach: true,
				        dots: true,
				        nav: false,
				        //loop: true,
				        lazyLoad: true,
				        mouseDrag: false,
				        autoplay:true,
					    autoplayTimeout:10000,
					    autoplayHoverPause:true,
					    animateOut: 'fadeOut',
					    smartSpeed: 450,
					    autoplaySpeed: 4000
				    });


			    /*****************************************
			        *  CARROSSEL DA PÁGINA PROJETO IMAGENS
			    *****************************************/
				    $("#carrossel-imagens-projeto").owlCarousel({
				        items : 4,
				        nav: false,
				        //loop: true,
				        lazyLoad: true,
				        mouseDrag: true,
				        autoplay:true,
					    autoplayTimeout:10000,
					    autoplayHoverPause:true,
					    animateOut: 'fadeOut',
					    smartSpeed: 450,
					    autoplaySpeed: 4000,
					    responsiveClass:true,
					        responsive:{
					            0:{
					                items:1
					            },
					            600:{
					                items:3
					            },
					            1000:{
					                items:4
					            }
					        }
				    });


				   var carrosselimgproj = $("#carrossel-imagens-projeto").data('owlCarousel');

					$('.navegacaoCarrosselProjeTras').click(function(){ carrosselimgproj.prev(); });
					$('.navegacaoCarrosselProjeFrente').click(function(){ carrosselimgproj.next(); });

			    /*****************************************
			        *  CARROSSEL DA PÁGINA QUEM SOMOS IMAGENS
			    *****************************************/

					$("#carrossel-quem-somos").owlCarousel({
				        items : 4,
				        nav: false,
				        loop: true,
				        lazyLoad: true,
				        mouseDrag: true,
				        autoplay:true,
					    autoplayTimeout:10000,
					    autoplayHoverPause:true,
					    animateOut: 'fadeOut',
					    smartSpeed: 450,
					    autoplaySpeed: 4000,
					    responsiveClass:true,
					        responsive:{
					            0:{
					                items:1
					            },
					            600:{
					                items:3
					            },
					            1000:{
					                items:4
					            }
					        }
				    });


				   var carrosselimgquem = $("#carrossel-quem-somos").data('owlCarousel');

					$('.navegacaoCarrosselQuemTras').click(function(){ carrosselimgquem.prev(); });
					$('.navegacaoCarrosselQuemFrente').click(function(){ carrosselimgquem.next(); });

					console.log(carrosselimgquem);
		    /*****************************************
		        *  	ANIMAÇÃO ITEMS ON SCROLL
		    *****************************************/
			    $(window).scroll( function(){

			        /* Check the location of each desired element */
			        $('.item-vantagem').each( function(i){

			            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
			            var bottom_of_window = $(window).scrollTop() + $(window).height();

			            /* If the object is completely visible in the window, fade it it */
			            if( bottom_of_window > bottom_of_object ){
			            	 $(this).css('opacity', '1');
			                $(this).addClass( "fadeInLeft animated" );

			            }

			        });

			        $('.item-vantagem2').each( function(i){

			            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
			            var bottom_of_window = $(window).scrollTop() + $(window).height();

			            /* If the object is completely visible in the window, fade it it */
			            if( bottom_of_window > bottom_of_object ){
			            	 $(this).css('opacity', '1');
			                $(this).addClass( "fadeInRight animated" );

			            }

			        });

			    });

		$("a#example1").fancybox({
			'titleShow'     : false
		});
	});