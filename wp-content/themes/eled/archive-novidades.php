<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eled_site
 */

get_header(); ?>

<!-- PÁGINAS NOVIDADES  -->
	<div class="pg pg-novidades" >

		<div class="imagem-top topo-novidades">
			<div class="efeito-sombra"></div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="titulo-internas">
					<span>Novidades <b>Eled</b></span>
				</div>
			</div>
		</div>

		<div class="container">

			<!-- FRASE TOPO NOVIDADES -->
			<div class="row frase-novidades">

				<div class="col-md-12 correcao-x text-center">
					<p>Lorem ipsum dolor amet consectetur adipiscing sollicitudin commodo <b>novidades</b>.</p>
				</div>

			</div>

			<!-- MENU LATERAL -->
			<div class="row novidades">

				<div class="col-sm-3">

					<div class="panel-group" id="accordion" aria-multiselectable="true">

						<!-- OPÇÃO MENU -->
						<div class="panel">
							<div class="panel-heading" id="headingOne">
								<div class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Arquivos 2015 <i class="fa fa-angle-double-right"></i></a>
								</div>
							</div>
							<div id="collapseOne" class="panel-collapse collapse" aria-labelledby="headingOne">
								<div class="panel-body">
									<ul>
										<li><a href="#" title="">Janeiro</a></li>
										<li><a href="#" title="">Fevereiro</a></li>
										<li><a href="#" title="">Março</a></li>
										<li><a href="#" title="">Abril</a></li>
										<li><a href="#" title="">Maio</a></li>
										<li><a href="#" title="">Junho</a></li>
										<li><a href="#" title="">Julho</a></li>
										<li><a href="#" title="">Agosto</a></li>
										<li><a href="#" title="">Setembro</a></li>
										<li><a href="#" title="">Outubro</a></li>
										<li><a href="#" title="">Novembro</a></li>
										<li><a href="#" title="">Dezembro</a></li>
									</ul>
								</div>
							</div>
						</div>

						<!-- OPÇÃO MENU -->
						<div class="panel">
							<div class="panel-heading" id="headingTwo">
								<div class="panel-title">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Arquivos 2014 <i class="fa fa-angle-double-right"></i></a>
								</div>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse" aria-labelledby="headingTwo">
								<div class="panel-body">
									<ul>
										<li><a href="#" title="">Janeiro</a></li>
										<li><a href="#" title="">Fevereiro</a></li>
										<li><a href="#" title="">Março</a></li>
										<li><a href="#" title="">Abril</a></li>
										<li><a href="#" title="">Maio</a></li>
										<li><a href="#" title="">Junho</a></li>
										<li><a href="#" title="">Julho</a></li>
										<li><a href="#" title="">Agosto</a></li>
										<li><a href="#" title="">Setembro</a></li>
										<li><a href="#" title="">Outubro</a></li>
										<li><a href="#" title="">Novembro</a></li>
										<li><a href="#" title="">Dezembro</a></li>
									</ul>
								</div>
							</div>
						</div>

						<!-- OPÇÃO MENU -->
						<div class="panel">
							<div class="panel-heading" id="headingThree">
								<div class="panel-title">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Outros Arquivos <i class="fa fa-angle-double-right"></i></a>
								</div>
							</div>
							<div id="collapseThree" class="panel-collapse collapse" aria-labelledby="headingThree">
								<div class="panel-body">
									<ul>
										<li><a href="#" title="">Janeiro</a></li>
										<li><a href="#" title="">Fevereiro</a></li>
										<li><a href="#" title="">Março</a></li>
										<li><a href="#" title="">Abril</a></li>
										<li><a href="#" title="">Maio</a></li>
										<li><a href="#" title="">Junho</a></li>
										<li><a href="#" title="">Julho</a></li>
										<li><a href="#" title="">Agosto</a></li>
										<li><a href="#" title="">Setembro</a></li>
										<li><a href="#" title="">Outubro</a></li>
										<li><a href="#" title="">Novembro</a></li>
										<li><a href="#" title="">Dezembro</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>

				</div>

				<div class="col-sm-9">

						<div id="novidades-container" class="novidades-container">

							<ul class="novidades-grid">

								<!-- NOVIDADE -->
								<li>

									<a href="#" title="">
										<div class="col-md-4">

											<div class="imagem" style="background: url(img/post1.jpg)">

											</div>
											<div class="data">

												<div class="row">
													<div class="col-xs-5 calendario">
														<span></span>
													</div>
													<div class="col-xs-7 data-texto">
														<strong> 27|Outubro</strong>
													</div>
												</div>

											</div>
											<div class="row descricao">
												<div class="col-xs-10">
													<div class="">
														<span>Proin hendrerit tincidunt turpis porttitor varius. </span>
													</div>
												</div>
												<div class="col-xs-2">
													<i class="fa fa-angle-double-right"></i>
												</div>
											</div>

										</div>
									</a>

								</li>

								<!-- NOVIDADE -->
								<li>

									<a href="#" title="">
										<div class="col-md-4">

											<div class="imagem" style="background: url(img/post1.jpg)">

											</div>
											<div class="data">

												<div class="row">
													<div class="col-xs-5 calendario">
														<span></span>
													</div>
													<div class="col-xs-7 data-texto">
														<strong> 27|Outubro</strong>
													</div>
												</div>

											</div>
											<div class="row descricao">
												<div class="col-xs-10">
													<div class="">
														<span>Proin hendrerit tincidunt turpis porttitor varius. </span>
													</div>
												</div>
												<div class="col-xs-2">
													<i class="fa fa-angle-double-right"></i>
												</div>
											</div>

										</div>
									</a>

								</li>

								<!-- NOVIDADE -->
								<li>

									<a href="#" title="">
										<div class="col-md-4">

											<div class="imagem" style="background: url(img/post1.jpg)">

											</div>
											<div class="data">

												<div class="row">
													<div class="col-xs-5 calendario">
														<span></span>
													</div>
													<div class="col-xs-7 data-texto">
														<strong> 27|Outubro</strong>
													</div>
												</div>

											</div>
											<div class="row descricao">
												<div class="col-xs-10">
													<div class="">
														<span>Proin hendrerit tincidunt turpis porttitor varius. </span>
													</div>
												</div>
												<div class="col-xs-2">
													<i class="fa fa-angle-double-right"></i>
												</div>
											</div>

										</div>
									</a>

								</li>

								<!-- NOVIDADE -->
								<li>

									<a href="#" title="">
										<div class="col-md-4">

											<div class="imagem" style="background: url(img/post1.jpg)">

											</div>
											<div class="data">

												<div class="row">
													<div class="col-xs-5 calendario">
														<span></span>
													</div>
													<div class="col-xs-7 data-texto">
														<strong> 27|Outubro</strong>
													</div>
												</div>

											</div>
											<div class="row descricao">
												<div class="col-xs-10">
													<div class="">
														<span>Proin hendrerit tincidunt turpis porttitor varius. </span>
													</div>
												</div>
												<div class="col-xs-2">
													<i class="fa fa-angle-double-right"></i>
												</div>
											</div>

										</div>
									</a>

								</li>

								<!-- NOVIDADE -->
								<li>

									<a href="#" title="">
										<div class="col-md-4">

											<div class="imagem" style="background: url(img/post1.jpg)">

											</div>
											<div class="data">

												<div class="row">
													<div class="col-xs-5 calendario">
														<span></span>
													</div>
													<div class="col-xs-7 data-texto">
														<strong> 27|Outubro</strong>
													</div>
												</div>

											</div>
											<div class="row descricao">
												<div class="col-xs-10">
													<div class="">
														<span>Proin hendrerit tincidunt turpis porttitor varius. </span>
													</div>
												</div>
												<div class="col-xs-2">
													<i class="fa fa-angle-double-right"></i>
												</div>
											</div>

										</div>
									</a>

								</li>

								<!-- NOVIDADE -->
								<li>

									<a href="#" title="">
										<div class="col-md-4">

											<div class="imagem" style="background: url(img/post1.jpg)">

											</div>
											<div class="data">

												<div class="row">
													<div class="col-xs-5 calendario">
														<span></span>
													</div>
													<div class="col-xs-7 data-texto">
														<strong> 27|Outubro</strong>
													</div>
												</div>

											</div>
											<div class="row descricao">
												<div class="col-xs-10">
													<div class="">
														<span>Proin hendrerit tincidunt turpis porttitor varius. </span>
													</div>
												</div>
												<div class="col-xs-2 text-right">
													<i class="fa fa-angle-double-right"></i>
												</div>
											</div>

										</div>
									</a>

								</li>

							</ul>

						</div>

				</div>

			</div>

		</div>

	</div>



<?php get_footer(); ?>
