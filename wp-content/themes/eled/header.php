<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package eled_site
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:url" content="" />
<meta property="og:image" content=""/>
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<!-- FAVICON -->
<link rel="shortcut icon" type="image/x-icon" href=" <?php echo get_template_directory_uri(); ?>/favicon.ico">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="site">
		<header class="area-topo">

		<!-- CARROSSEL FUNDO -->
		<div class="carrossel" style="<?php if(is_front_page()){ echo "display: block;";}else{echo "display: none;";}?>">

			<div id="carrossel-inicial" class="owl-carousel">
				<?php //if ( have_posts() ) : ?>

				<?php

					$destaquesPost = new WP_Query( array( 'post_type' => 'destaques', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );

                    while ( $destaquesPost->have_posts() ) : $destaquesPost->the_post();

					$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$foto = $foto[0];

				?>

					<!-- ITEM -->
					<div class="item" data-id="0" style="background: url(<?php echo "$foto"; ?>)">
						<div class="overlay"></div>
						<div class="container text-center">
							<div class="texto-carrossel">
								<span><?php echo rwmb_meta('Eled_destaques_subtitulo'); ?></span>
								<h2><?php echo get_the_content(); ?></h2>
								<?php echo '<a href="' . rwmb_meta('Eled_destaques_link') . '" title="' . __('Continue Reading ', 'eled') . get_the_title() . '" rel="bookmark">'; ?>Clique e <b>conheça</b></a>
							</div>
						</div>

					</div>
					<?php endwhile; wp_reset_query(); ?>
				<?php //endif; ?>
		</div>

			<!-- BOTÕES DE NAVEGAÇÃO -->
			<button class="navegacaoCarrosselTopFrente hidden-xs"><i class="fa fa-angle-left"></i></button>
			<button class="navegacaoCarrosselTopTras hidden-xs"><i class="fa fa-angle-right"></i></button>

		</div>

		<!-- CABEÇALHO -->
		<header class="topo">
			<nav class="navbar" role="navigation">

				<div class="">

					<!-- LOGOTIPO / MENU MOBILE-->
					<div class="navbar-header">
						<!-- LOGO -->
						<a href="<?php echo home_url('/index.php'); ?>" title="Eled Light">
							<h1>
								Eled Light
							</h1>
						</a>

						<!-- MENU MOBILE TRIGGER -->
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
							<span class="sr-only"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

					</div>

					<!-- MENU -->
					<nav class="collapse navbar-collapse" id="collapse">
						<?php
							$pageurl = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
							switch ($pageurl) {
									case 'localhost/projetos/eled_site/':
										$index ='link-ativo';
										break;

									case 'localhost/projetos/eled_site/projetos/':
										$projetos ='link-ativo';
										break;

									case 'localhost/projetos/eled_site/nossos-fornecedores/':
										$fornecedores ='link-ativo';
										break;

									case 'localhost/projetos/eled_site/nossos-parceiros/':
										$parceiros ='link-ativo';
										break;



									case 'localhost/projetos/eled_site/quem-somos/':
										$quemsomos ='link-ativo';
										break;

									case 'localhost/projetos/eled_site/novidades/':
										$novidades ='link-ativo';
										break;

									case 'localhost/projetos/eled_site/contato/':
										$contato ='link-ativo';
										break;
									default:
										# code...
										break;
								}

						 ?>
						<ul class="nav navbar-nav">

							<li class="<?php echo $index; ?>"><a href="<?php echo home_url('/index.php'); ?>">Ínicio</a></li>
							<li class="<?php echo $projetos; ?>" ><a href="<?php echo home_url('/index.php/projetos/'); ?>" >Projetos</a></li>
							<li class="<?php echo $fornecedores; ?>"><a href="<?php echo home_url('/index.php/nossos-fornecedores/'); ?>" >Fornecedores</a></li>
							<li class="<?php echo $parceiros; ?>"><a href="<?php echo home_url('/index.php/nossos-parceiros/'); ?>" >Parceiros</a></li>
							<li class="<?php echo $quemsomos; ?>"><a href="<?php echo home_url('/index.php/quem-somos/'); ?>" >Quem somos</a></li>
							<li class="<?php echo $novidades; ?>"><a href="<?php echo home_url('/index.php/novidades/'); ?>" >Novidades</a></li>
							<li class="<?php echo $contato; ?>"><a href="<?php echo home_url('/index.php/contato/'); ?>" >Contato</a></li>

						</ul>

					</nav>

				</div>
			</nav>
		</header>








	</div>


	<div id="content" class="site-content">
