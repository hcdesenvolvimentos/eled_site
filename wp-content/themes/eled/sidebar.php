<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package eled_site
 */

// if ( ! is_active_sidebar( 'sidebar-1' ) ) {
// 	return;
// }
?>

<!-- <div id="secondary" class="widget-area" role="complementary">
	<?php //dynamic_sidebar( 'sidebar-1' ); ?>
</div> -->

<div class="row novidades">

	<div class="col-sm-3">

		<div class="panel-group" id="accordion" aria-multiselectable="true">

			<?php
					/* Seleciona os anos no banco de dados */
					$anos = $wpdb->get_col("SELECT DISTINCT YEAR(post_date) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'post' ORDER BY post_date DESC");
					$i = 0;
					foreach($anos as $ano) :
			?>

			<!-- OPÇÃO MENU -->
			<div class="panel">
				<div class="panel-heading" id="headingTwo">
					<div class="panel-title">
						<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo<?php echo $i; ?>" aria-expanded="false" aria-controls="collapseTwo">Arquivos <?php echo $ano; ?> <i class="fa fa-angle-double-right"></i></a>
					</div>
				</div>
				<div id="collapseTwo<?php echo $i; ?>" class="panel-collapse collapse" aria-labelledby="headingTwo">
					<div class="panel-body">
						<ul>

							<?php

									$args = array(
													'type' => 'monthly',
													'echo' => 0,
													'year' => ''.$ano.'',
													);


									echo wp_get_archives( $args );

							?>
						</ul>
					</div>
				</div>
			</div>

			<?php  $i++; endforeach; ?>

		</div>


	</div>

	<div class="col-sm-9">

			<div id="novidades-container" class="novidades-container">

				<ul class="novidades-grid">
				<?php if ( have_posts() ) :

						while ( have_posts() ) : the_post();
						
				?>

                    <?php

                        $foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                        $foto = $foto[0];


                     ?>

					<!-- NOVIDADE -->
					<li>

						<a href="<?php echo get_permalink(); ?>" title="">
							<div class="col-md-4">

								<div class="imagem" style="background: url(<?php echo "$foto"; ?>)">

								</div>
								<div class="data">

									<div class="row">
										<div class="col-xs-5 calendario">
											<span></span>
										</div>
										<div class="col-xs-7 data-texto">
											<strong> <?php the_time('j \d\e F') ?></strong>
										</div>
									</div>

								</div>
								<div class="row descricao">
									<div class="col-xs-10">
										<div class="">
											<h2><?php echo get_the_title(); ?></h2>
										</div>
									</div>
									<div class="col-xs-2">
										<i class="fa fa-angle-double-right"></i>
									</div>
								</div>

							</div>
						</a>

					</li>

					<?php endwhile; ?>

                <?php endif; ?>

				</ul>

			</div>

	</div>

</div>
