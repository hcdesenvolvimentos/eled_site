<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package eled_site
 */

get_header(); ?>

<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package eled_site
 */

get_header(); ?>

	<div id="primary" class="content-area">
	<!-- PÁGINA NOVIDADE  -->
	<div class="pg pg-novidade">

		<div class="imagem-top topo-novidades">
			<div class="efeito-sombra"></div>
		</div>

		<!-- TÍTULO -->
		<div class="row">
			<div class="col-md-12">
				<div class="titulo-internas">
					<span>Novidades <b>Eled</b></span>
				</div>
			</div>
		</div>

		<div class="container">

			<!-- FRASE TOPO NOVIDADES -->
			<div class="row frase-novidades">

				<div class="col-md-12 correcao-x text-center">
					<!-- <p>Lorem ipsum dolor amet consectetur adipiscing sollicitudin commodo <b>novidades</b>.</p> -->
				</div>

			</div>

			<!-- CABEÇALHO NOVIDADE -->
			<div class="row novidade">
				<div class="col-md-12">
					<?php if ( have_posts() ) : while( have_posts() ) : the_post();
                		$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	                    $foto = $foto[0];
					endwhile; endif; ?>



					<div class="cabecalho img-responsive" style="background: url(<?php echo $foto ?>);">
						<div class="dados">
							<div class="data">
								<img src="<?php echo get_template_directory_uri(); ?>/img/calendari-o.png">
								<span><?php the_time('j') ?> | <?php the_time('F') ?></span>
							</div>
							<div class="informacao">
								<?php the_title();?>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- TEXTO NOVIDADE -->
			<div class="row texto">

				<div class="col-md-8 col-md-offset-2">
					<p>
						<?php if ( have_posts() ) : while( have_posts() ) : the_post();

						echo get_the_content();
						
                    	endwhile; endif; ?>
                    </p>
					<i>Curitiba, <?php the_time('j \d\e F \d\e Y') ?> - Eled Light</i>
				</div>

			</div>

			<!-- OUTRAS NOVIDADES -->
			<div class="row outras-novidades">

				<div class="overlay-preto">

					<div class="col-md-8 col-md-offset-2">
						<p>Confira nossas outras postagens</p>

						<span><?php previous_post('%','<i class="fa fa-angle-double-left"></i> Publicação anterior ', 'no') ?></span>
						<span><?php next_post('%','Proxima publicação <i class="fa fa-angle-double-right"></i>','no')?></span>
					</div>
				</div>

			</div>


		</div>

	</div>


	</div><!-- #primary -->




<?php get_footer(); ?>



<?php get_footer(); ?>
