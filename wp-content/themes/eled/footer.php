<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Eled
 */

global $configuracao;



?>




	<div class="site-info">
			<!-- RODAPÉ -->
		<!-- RODAPÉ -->
	<footer>

		<div class="rodape">

				<!-- MAPA -->
				<div class="row">
					<div class="col-md-12">
						<div class="mapa">

							<iframe src="https://maps.google.com.br/maps?q=<?php echo $configuracao['opt-ruaNumero']; ?>&output=embed"  frameborder="0" style="border:0" allowfullscreen></iframe>

						</div>
					</div>
				</div>

				<!-- INFORMAÇÕES DE CONTATO -->
				<div class="imagem-de-fundo">
					<div class="row">
						<div class="col-md-4">
							<div class="titulo">Onde <b>Estamos?</b></div>
							<span><img src="<?php echo get_template_directory_uri(); ?>/img/tel.png"><?php echo $configuracao['opt-telefone']; ?></span>
							<span><img src="<?php echo get_template_directory_uri(); ?>/img/email.png"> <?php echo $configuracao['opt-emailContato']; ?></span>
							<span><img src="<?php echo get_template_directory_uri(); ?>/img/endereco.png">
								<?php echo $configuracao['opt-ruaNumero']; ?>
							</span>
						</div>

						<!-- ÍNCONES REDES SOCIAIS -->
						<div class="col-md-4">
							<div class="incone-logo">
								<div class="logo-footer">
									<img src="<?php echo get_template_directory_uri(); ?>/img/logo-footer.png">
								</div>
								<a href="<?php echo $configuracao['opt-instagram'] ?> " title="instagram" target="_blank"><i class="fa fa-instagram"></i></a>
								<a href="<?php echo $configuracao['opt-twitter'] ?>" title="twitter" target="_blank"><i class="fa fa-twitter"></i></a>
								<a href="<?php echo $configuracao['opt-google'] ?>" title="google plus" target="_blank"><i class="fa fa-google-plus"></i></a>
								<a href="<?php echo $configuracao['opt-facebook'] ?>" title="facebook" target="_blank"><i class="fa fa-facebook"></i></a>
							</div>
						</div>

						<!-- FORMULÁRIO DE CONTATO -->
						<div class="col-md-4">
							<div class="formulario">
								<span>RECEBA NOVIDADES ! <img src="<?php echo get_template_directory_uri(); ?>/img/aviao.png"></span>
								<?php
                       				//echo do_shortcode('[contact-form-7 id="103" title="formulário ínicial"]');
                        		?>
                        		<!--START Scripts : this is the script part you can add to the header of your theme-->
                        		<script type="text/javascript" src="http://eled.pixd.com.br/wp-includes/js/jquery/jquery.js?ver=2.6.19"></script>
                        		<script type="text/javascript" src="http://eled.pixd.com.br/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.6.19"></script>
                        		<script type="text/javascript" src="http://eled.pixd.com.br/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.6.19"></script>
                        		<script type="text/javascript" src="http://eled.pixd.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.6.19"></script>
                        		<script type="text/javascript">
                        		    /* <![CDATA[ */
                        		    var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://eled.pixd.com.br/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
                        		    /* ]]> */
                        		</script><script type="text/javascript" src="http://eled.pixd.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.6.19"></script>
                        		<!--END Scripts-->

                        		<div class="widget_wysija_cont html_wysija"><div id="msg-form-wysija-html566f0e80bd664-1" class="wysija-msg ajax validated[abs][firstname]"></div><form id="form-wysija-html566f0e80bd664-1" method="post" action="#wysija" class="widget_wysija html_wysija">
                        		<p class="wysija-paragraph">
                        		    <label for="seunome" class="hidden">Nome</label>

                        		    <input type="text" name="wysija[user][firstname]" class="form-control wysija-input " title="Nome"  value="" placeholder="nome"/>



                        		    <span class="abs-req">
                        		        <input type="text" name="wysija[user][abs][firstname]" class="form-control wysija-input validated[abs][firstname]" value="" placeholder="nome"/>
                        		    </span>

                        		</p>
                        		<p class="wysija-paragraph">
                        		    <label for="seuemail" class="hidden">Email</label>

                        		    <input type="text" name="wysija[user][email]" class="form-control wysija-input validate[required,custom[email]]" title="Email"  value="" placeholder="email"/>



                        		    <span class="abs-req">
                        		        <input type="text" name="wysija[user][abs][email]" class="form-control wysija-input validated[abs][email]" value="" placeholder="email"/>
                        		    </span>

                        		</p>
                        		<input class="botao wysija-submit wysija-submit-field" type="submit" value="Assinar!" />

                        		<input type="hidden" name="form_id" value="1" />
                        		<input type="hidden" name="action" value="save" />
                        		<input type="hidden" name="controller" value="subscribers" />
                        		<input type="hidden" value="1" name="wysija-page" />


                        		<input type="hidden" name="wysija[user_list][list_ids]" value="1" />

                        		</form></div>
							</div>

						</div>
					</div>

				</div>
			<div class="termos">
				<div class="row">
					<div class="container">
						<div class="col-md-6">
							<small>© Copyright 2015</small> Eled Light - Todos os direitos reservados.
						</div>
						<div class="col-md-6 text-right">
							<small>
							    <span style="display:inline;color:#fff;">Desenvolvido por</span>
							    <a href="http://palupa.com.br/" target="_blank" title="Desenvolvido por Palupa Marketing" style="display: inline";> <img src="<?php echo get_template_directory_uri(); ?>/img/palupaLogoCinza.png" style="max-height: 15px;"></a>
							</small>
						</div>	
					</div>
				</div>
			</div>
		</div>

	</footer>

	</div><!-- .site-info -->



<?php wp_footer(); ?>

</body>
</html>
