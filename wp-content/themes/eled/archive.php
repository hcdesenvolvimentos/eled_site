<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eled_site
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			
			<!-- PÁGINAS NOVIDADES  -->
			<div class="pg pg-novidades">

				<div class="imagem-top topo-novidades">
					<div class="efeito-sombra"></div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="titulo-internas">
							<span>Novidades <b>Eled</b></span>
						</div>
					</div>
				</div>

				<div class="container">

					<!-- FRASE TOPO NOVIDADES -->
					<div class="row frase-novidades">

						<div class="col-md-12 correcao-x text-center">
							<p>Lorem ipsum dolor amet consectetur adipiscing sollicitudin commodo <b>novidades</b>.</p>
						</div>
					</div>

					<!-- MENU LATERAL -->
					<?php get_sidebar(); ?>

				</div>	
			
		</div>
	</div>

<?php get_footer(); ?>