<?php

/**
 * Plugin Name: Base Eled
 * Description: Controle base do tema Eled.
 * Version: 0.1
 * Author: Agência Palupa
 * Author URI: http://www.palupa.com.br
 * Licence: GPL2
 */

	function baseEled () {

		// TIPOS DE CONTEÚDO
		conteudosEled();

		// TAXONOMIA
		taxonomiaEled();

		// META BOXES
		metaboxesEled();

		// SHORTCODES
		// shortcodesEled();

	    // ATALHOS VISUAL COMPOSER
	    //visualcomposerEled();

	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosEled (){

		// TIPOS DE CONTEÚDO
		tipoDestaques();
		tipoParceiros();
		tipoFornecedores();
		tipoCliente();
		tipoDepoimentos();
		tipoProjetos();

		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaques':
					$titulo = 'Nome do destaque';
				break;

				case 'parceiros':
					$titulo = 'Nome do parceiro';
				break;

				case 'fornecedores':
					$titulo = 'Nome do fornecedor';
				break;

				case 'depoimentos':
					$titulo = 'Nome da pessoa';
				break;

				case 'projetos':
					$titulo = 'Nome da projeto';
				break;

				case 'cliente':
					$titulo = 'Nome da cliente';
				break;

				default:
				break;
			}

		    return $titulo;

		}

	}

		function tipoDestaques() {

			$rotulosDestaques = array(
									'name'               => 'Destaques',
									'singular_name'      => 'Destaque',
									'menu_name'          => 'Destaques',
									'name_admin_bar'     => 'Destaques',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo destaque',
									'new_item'           => 'Novo destaque',
									'edit_item'          => 'Editar destaque',
									'view_item'          => 'Ver destaque',
									'all_items'          => 'Todos os destaques',
									'search_items'       => 'Buscar destaques',
									'parent_item_colon'  => 'Dos destaques',
									'not_found'          => 'Nenhum destaque cadastrado.',
									'not_found_in_trash' => 'Nenhum destaque na lixeira.'
								);

			$argsDestaques 	= array(
									'labels'             => $rotulosDestaques,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-format-gallery',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'destaques' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('destaques', $argsDestaques);

		}

		function tipoParceiros() {

			$rotulosParceiros = array(
									'name'               => 'Parceiros',
									'singular_name'      => 'Parceiro',
									'menu_name'          => 'Parceiros',
									'name_admin_bar'     => 'Parceiros',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo parceiro',
									'new_item'           => 'Novo parceiro',
									'edit_item'          => 'Editar parceiro',
									'view_item'          => 'Ver parceiro',
									'all_items'          => 'Todos os parceiros',
									'search_items'       => 'Buscar parceiros',
									'parent_item_colon'  => 'Dos parceiros',
									'not_found'          => 'Nenhum parceiro cadastrado.',
									'not_found_in_trash' => 'Nenhum parceiro na lixeira.'
								);

			$argsParceiros 	= array(
									'labels'             => $rotulosParceiros,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-groups',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'parceiros' ),
									'capability_type'    => 'post',
									'has_archive'        => false,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('parceiros', $argsParceiros);

		}

		function tipoFornecedores() {

			$rotulosFornecedores = array(
									'name'               => 'Fornecedores',
									'singular_name'      => 'Fornecedor',
									'menu_name'          => 'Fornecedores',
									'name_admin_bar'     => 'Fornecedores',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo fornecedor',
									'new_item'           => 'Novo fornecedor',
									'edit_item'          => 'Editar fornecedor',
									'view_item'          => 'Ver fornecedor',
									'all_items'          => 'Todos os fornecedores',
									'search_items'       => 'Buscar fornecedores',
									'parent_item_colon'  => 'Dos fornecedores',
									'not_found'          => 'Nenhum fornecedor cadastrado.',
									'not_found_in_trash' => 'Nenhum fornecedor na lixeira.'
								);

			$argsFornecedores 	= array(
									'labels'             => $rotulosFornecedores,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-clipboard',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'fornecedores' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('fornecedores', $argsFornecedores);

		}
		function tipoCliente() {

		$rotulosCliente = array(
								'name'               => 'Clientes',
								'singular_name'      => 'cliente',
								'menu_name'          => 'Clientes',
								'name_admin_bar'     => 'Clientes',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo cliente',
								'new_item'           => 'Novo cliente',
								'edit_item'          => 'Editar cliente',
								'view_item'          => 'Ver cliente',
								'all_items'          => 'Todos os clientes',
								'search_items'       => 'Buscar cliente',
								'parent_item_colon'  => 'Dos clientees',
								'not_found'          => 'Nenhum cliente cadastrado.',
								'not_found_in_trash' => 'Nenhum cliente na lixeira.'
							);

		$argsCliente 	= array(
								'labels'             => $rotulosCliente,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-clipboard',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'fornecedores' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title', 'thumbnail', 'editor' )
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('cliente', $argsCliente);

		}
		function tipoDepoimentos() {

			$rotulosDepoimentos = array(
									'name'               => 'Depoimentos',
									'singular_name'      => 'Depoimento',
									'menu_name'          => 'Depoimentos',
									'name_admin_bar'     => 'Depoimentos',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo depoimento',
									'new_item'           => 'Novo depoimento',
									'edit_item'          => 'Editar depoimento',
									'view_item'          => 'Ver depoimento',
									'all_items'          => 'Todos os depoimentos',
									'search_items'       => 'Buscar depoimentos',
									'parent_item_colon'  => 'Dos depoimentos',
									'not_found'          => 'Nenhum depoimento cadastrado.',
									'not_found_in_trash' => 'Nenhum depoimento na lixeira.'
								);

			$argsDepoimentos 	= array(
									'labels'             => $rotulosDepoimentos,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-format-chat',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'depoimentos' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('depoimentos', $argsDepoimentos);

		}

		function tipoProjetos() {

			$rotulosProjetos = array(
									'name'               => 'Projetos',
									'singular_name'      => 'Projeto',
									'menu_name'          => 'Projetos',
									'name_admin_bar'     => 'Projetos',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo projeto',
									'new_item'           => 'Novo projeto',
									'edit_item'          => 'Editar projeto',
									'view_item'          => 'Ver projeto',
									'all_items'          => 'Todos os projetos',
									'search_items'       => 'Buscar projeto',
									'parent_item_colon'  => 'Dos projetos',
									'not_found'          => 'Nenhum projeto projeto.',
									'not_found_in_trash' => 'Nenhum projeto na lixeira.'
								);

			$argsProjetos 	= array(
									'labels'             => $rotulosProjetos,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-lightbulb',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'projetos' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('projetos', $argsProjetos);

		}

	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaEled () {

		taxonomiaCategoriaProjetos();

	}

		function taxonomiaCategoriaProjetos() {

			$rotulosCategoriaProjetos = array(
												'name'              => 'Categorias de projetos',
												'singular_name'     => 'Categoria de projetos',
												'search_items'      => 'Buscar categorias de projetos',
												'all_items'         => 'Todas categorias de projetos',
												'parent_item'       => 'Categoria de projetos pai',
												'parent_item_colon' => 'Categoria de projetos pai:',
												'edit_item'         => 'Editar categoria de projetos',
												'update_item'       => 'Atualizar categoria de projetos',
												'add_new_item'      => 'Nova categoria de projetos',
												'new_item_name'     => 'Nova categoria',
												'menu_name'         => 'Categorias de projetos',
											);

			$argsCategoriaProjetos 		= array(
												'hierarchical'      => true,
												'labels'            => $rotulosCategoriaProjetos,
												'show_ui'           => true,
												'show_admin_column' => true,
												'query_var'         => true,
												'rewrite'           => array( 'slug' => 'categoria-projetos' ),
											);

			register_taxonomy( 'categoriaProjetos', array( 'projetos' ), $argsCategoriaProjetos );

		}

    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesEled(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'Eled_';

			// CARROSSEL DE DESTAQUES PÁGINA INICIAL
			$metaboxes[] = array(

				'id'			=> 'destaqueMetabox',
				'title'			=> 'Detalhes do destaque',
				'pages' 		=> array( 'destaques' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Subtítulo do destaque',
						'id'    => "{$prefix}destaques_subtitulo",
						'desc'  => '',
						'type'  => 'text'
					),

					array(
						'name'  => 'Link do destaque',
						'id'    => "{$prefix}destaques_link",
						'desc'  => '',
						'type'  => 'text'
					),

				),
				'validation' 	=> array()
			);

			// PROJETOS
			$metaboxes[] = array(

				'id'			=> 'projetosMetabox',
				'title'			=> 'Detalhes do Projeto',
				'pages' 		=> array( 'projetos' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Trabalho realizado',
						'id'    => "{$prefix}projetos_trabalho_realizado",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Detalhes do cliente',
						'id'    => "{$prefix}projetos_detalhe_cliente",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Fotos do projeto para galeria',
						'id'    => "{$prefix}projetos_foto",
						'desc'  => '',
						'type'  => 'plupload_image'
					),
					array(
						'name'  => 'Fotos plano de fundo do projeto',
						'id'    => "{$prefix}projetos_plano_fundo",
						'desc'  => '',
						'type'  => 'plupload_image'
					),


				),
				'validation' 	=> array()
			);

			// PARCEIROS
			$metaboxes[] = array(

				'id'			=> 'parceirosMetabox',
				'title'			=> 'Detalhes do parceiro',
				'pages' 		=> array( 'parceiros' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Link Parceiro',
						'id'    => "{$prefix}link_parceiros",
						'desc'  => '',
						'type'  => 'text'
					),

				),
				'validation' 	=> array()
			);


			// FORNECEDORES
			$metaboxes[] = array(

				'id'			=> 'fornecedoresMetabox',
				'title'			=> 'Detalhes do parceiro',
				'pages' 		=> array( 'fornecedores' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(


					array(
						'name'  => 'Link do fornecedor',
						'id'    => "{$prefix}link_fornecedores",
						'desc'  => '',
						'type'  => 'text'
					),

				),
				'validation' 	=> array()
			);

			// PÁGINAS GERAIS
			$metaboxes[] = array(

				'id'			=> 'pageMetabox',
				'title'			=> 'Detalhes da página',
				'pages' 		=> array( 'page' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Legenda inferior',
						'id'    => "{$prefix}legenda_imagem_lampada",
						'desc'  => '',
						'type'  => 'text',
					),

					array(
						'name'  => 'Frases adicionais da página',
						'id'    => "{$prefix}page_frases",
						'desc'  => '',
						'type'  => 'text',
						'clone' => true
					),

					array(
						'name'  			=> 'Galeria de imagens da página',
						'id'    			=> "{$prefix}page_foto",
						'desc'  			=> '',
						'type'  			=> 'plupload_image'
					)

				),
				'validation' 	=> array()
			);

			return $metaboxes;
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesEled(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerEled(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseEled');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	//add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseEled();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );